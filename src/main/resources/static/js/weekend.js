$(document).ready(function() {
    var form = $('#calculateWeekends');

    var updateResponseMessage = function() {
        $("#response-message").html('');
    };

    form.submit(function(event) {
        updateResponseMessage();
        event.preventDefault();
        ajaxPost();
    });

    function ajaxPost(){
        const INT_MAX = 2147483647;
        var startYearValue = $("#start-year").val();
        var endYearValue = $("#end-year").val();
        if (startYearValue > INT_MAX) {
            startYearValue = INT_MAX;
        }
        if (endYearValue > INT_MAX) {
            endYearValue = INT_MAX;
        }
        var formData = {
            startDay : $("#start-day").val(),
            startMonth : $("#start-month").val(),
            startYear :  startYearValue,
            endDay : $("#end-day").val(),
            endMonth : $("#end-month").val(),
            endYear :  endYearValue,
            includeHolidays : $("#include-holidays").prop('checked')
        };
        $.ajax({
            type : "POST",
            contentType : "application/json; charset=utf-8",
            url : "/api/calculate/weekends",
            data : JSON.stringify(formData),
            dataType : 'json',
            beforeSend: function() {
                $("#calculate").prop('disabled', true);
                $("#result").text('Process...');
            },
            success : function(response) {
                if (response.status === "OK") {
                    $("#calculate").prop('disabled', false);
                    $("#result").text(response.data);
                } else {
                    $("#result").text(0);
                    $("#response-message").text(response.errorMessage);
                    $("#calculate").prop('disabled', false);
                }
            }
        });

    }
})