package com.task.weekend.controller;

import com.task.weekend.model.CalculateWeekendsRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class MainController {

    @RequestMapping(value = {"/", "/weekend"}, method = RequestMethod.GET)
    public String weekend(Model model, Principal principal) {
        String name = principal.getName();
        model.addAttribute("login", name);
        model.addAttribute("calculateWeekendsRequest", new CalculateWeekendsRequest());
        return "weekend";
    }
}
