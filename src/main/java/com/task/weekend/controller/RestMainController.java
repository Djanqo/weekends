package com.task.weekend.controller;

import com.task.weekend.model.CalculateWeekendsRequest;
import com.task.weekend.model.Response;
import com.task.weekend.service.DateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.DateTimeException;

@RestController
@RequestMapping("/api/calculate")
public class RestMainController {

    @Autowired
    private DateService dateService;

    @PostMapping("/weekends")
    public Response calculateWeekends (@RequestBody CalculateWeekendsRequest calculateWeekendsRequest) {
        long numberOfWeekends;
        try {
            numberOfWeekends = dateService.getWeekendsBetweenTwoDates(calculateWeekendsRequest);
        } catch (DateTimeException e) {
            return Response.errorMessage(e.getMessage());
        }
        return Response.okMessage(numberOfWeekends);
    }
}
