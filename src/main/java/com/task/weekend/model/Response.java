package com.task.weekend.model;

public class Response {

    private String status;
    private Object data;
    private String errorMessage;

    public static Response okMessage(Object data) {
        return new Response("OK", data);
    }

    public static Response errorMessage(String message) {
        return new Response("ERROR", message);
    }

    private Response(String status, Object data) {
        this.status = status;
        this.data = data;
    }

    private Response(String status, String errorMessage) {
        this.status = status;
        this.errorMessage = errorMessage;
    }

    public String getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
