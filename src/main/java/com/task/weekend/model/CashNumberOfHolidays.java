package com.task.weekend.model;

public class CashNumberOfHolidays {

    private DayOfWeek januaryFirstDayOfWeek;

    private boolean leapYear;

    private int numberOfHolidays;

    public CashNumberOfHolidays() {
    }

    public CashNumberOfHolidays(DayOfWeek januaryFirstDayOfWeek, boolean leapYear, int numberOfHolidays) {
        this.januaryFirstDayOfWeek = januaryFirstDayOfWeek;
        this.leapYear = leapYear;
        this.numberOfHolidays = numberOfHolidays;
    }

    public DayOfWeek getJanuaryFirstDayOfWeek() {
        return januaryFirstDayOfWeek;
    }

    public void setJanuaryFirstDayOfWeek(DayOfWeek januaryFirstDayOfWeek) {
        this.januaryFirstDayOfWeek = januaryFirstDayOfWeek;
    }

    public boolean isLeapYear() {
        return leapYear;
    }

    public void setLeapYear(boolean leapYear) {
        this.leapYear = leapYear;
    }

    public int getNumberOfHolidays() {
        return numberOfHolidays;
    }

    public void setNumberOfHolidays(int numberOfHolidays) {
        this.numberOfHolidays = numberOfHolidays;
    }
}
