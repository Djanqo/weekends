package com.task.weekend.model;

public class Year {

    public static final int MIN_VALUE = 0;

    public static final int MAX_VALUE = 999999999;

    public static boolean isLeap(int year) {
        return ((year & 3) == 0) && ((year % 100) != 0 || (year % 400) == 0);
    }
}
