package com.task.weekend.model;

import java.time.DateTimeException;

public class Date implements Comparable<Date> {

    private final int year;

    private final short month;

    private final short day;

    private Date(int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = (short) month;
        this.day = (short) dayOfMonth;
    }

    public int getDayOfMonth() {
        return day;
    }

    public int getMonthValue() {
        return month;
    }

    public Month getMonth() {
        return Month.of(month);
    }

    public int getYear() {
        return year;
    }

    private static Date create(int year, int month, int dayOfMonth) {
        if (dayOfMonth > 28) {
            int dom = 31;
            switch (month) {
                case 2:
                    dom = (Year.isLeap(year) ? 29 : 28);
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    dom = 30;
                    break;
            }
            if (dayOfMonth > dom) {
                if (dayOfMonth == 29) {
                    throw new DateTimeException("Invalid date 'February 29' as '" + year + "' is not a leap year");
                } else {
                    throw new DateTimeException("Invalid date '" + Month.of(month).getDisplayName() + " " + dayOfMonth + "'");
                }
            }
        }
        return new Date(year, month, dayOfMonth);
    }

    public static Date of(int year, int month, int dayOfMonth) {
        if ((year < Year.MIN_VALUE) || (year > Year.MAX_VALUE)) {
            throw new DateTimeException("The year must be between " + Year.MIN_VALUE + " and " + Year.MAX_VALUE + " including!");
        }
        if ((month < 1) || (month > 12)) {
            throw new DateTimeException("Invalid value for MonthOfYear: " + month);
        }
        if ((dayOfMonth < 1) || (dayOfMonth > 31)) {
            throw new DateTimeException("Invalid value for DayOfMonth: " + dayOfMonth);
        }
        return create(year, month, dayOfMonth);
    }


    /**
     * Converts this date to the Epoch Day.
     * For example: day 0 is 0000-01-01
     *              day 366 is 0001-01-01
     *              day 731 is 0002-01-01
     *
     * @return the Epoch Day equivalent to this date
     */
    public long toEpochDay() {
        long y = year;
        long m = month;
        long total = 0;
        total += 365 * y;
        if (y >= 0) {
            total += (y + 3) / 4 - (y + 99) / 100 + (y + 399) / 400;
        } else {
            total -= y / -4 - y / -100 + y / -400;
        }
        total += ((367 * m - 362) / 12);
        total += day - 1;
        if (m > 2) {
            total--;
            if (Year.isLeap(year) == false) {
                total--;
            }
        }
        return total;
    }

    public static long daysBetweenTwoDates(Date start, Date end) {
        return end.toEpochDay() - start.toEpochDay();
    }

    public DayOfWeek getDayOfWeek() {
        int dow0 = (int) Math.floorMod(toEpochDay() + 5, 7);
        return DayOfWeek.of(dow0 + 1);
    }

    /**
     * Calculates weekends (Saturday, Sunday) between two dates.
     * The start and end dates are included in the calculation interval
     *
     * @param start  the date of the beginning
     * @param end  the date of the ending
     *
     * @return number of weekends
     */
    public static long weekendsBetweenTwoDates(Date start, Date end) {
        long daysBetween = Date.daysBetweenTwoDates(start, end) + 1;
        long totalWeeks = daysBetween / 7;
        long totalWeekends = totalWeeks * 2;
        long excess = daysBetween % 7;
        if (excess == 0) {
            return totalWeekends;
        }
        DayOfWeek startDayOfWeek = start.getDayOfWeek();
        if ((startDayOfWeek != DayOfWeek.SUNDAY) && ((startDayOfWeek.getValue() + excess) >= 8)) {
            totalWeekends += 2;
        }
        if (((startDayOfWeek.getValue() < 6) && ((startDayOfWeek.getValue() + excess) == 7))
                || ((excess == 1) && (startDayOfWeek.getValue() > 5)) || (startDayOfWeek == DayOfWeek.SUNDAY)) {
            totalWeekends += 1;
        }
        return totalWeekends;
    }

    private static Date resolvePreviousValid(int year, int month, int day) {
        switch (month) {
            case 2:
                day = Math.min(day, Year.isLeap(year) ? 29 : 28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                day = Math.min(day, 30);
                break;
        }
        return new Date(year, month, day);
    }

    public Date plusYear() {
        int newYear = year + 1;
        return resolvePreviousValid(newYear, month, day);
    }

    @Override
    public int compareTo(Date otherDate) {
        int cmp = (year - otherDate.year);
        if (cmp == 0) {
            cmp = (month - otherDate.month);
            if (cmp == 0) {
                cmp = (day - otherDate.day);
            }
        }
        return cmp;
    }

    public boolean isAfterOrEqual(Date other) {
        return compareTo(other) >= 0;
    }

    @Override
    public String toString() {
        return "Date{" +
                "year=" + year +
                ", month=" + month +
                ", day=" + day +
                '}';
    }
}
