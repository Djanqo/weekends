package com.task.weekend.model;

public class CalculateWeekendsRequest {

    private short startDay;

    private Month startMonth;

    private int startYear;

    private short endDay;

    private Month endMonth;

    private int endYear;

    private boolean includeHolidays;

    public short getStartDay() {
        return startDay;
    }

    public void setStartDay(short startDay) {
        this.startDay = startDay;
    }

    public Month getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Month startMonth) {
        this.startMonth = startMonth;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public short getEndDay() {
        return endDay;
    }

    public void setEndDay(short endDay) {
        this.endDay = endDay;
    }

    public Month getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(Month endMonth) {
        this.endMonth = endMonth;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public boolean isIncludeHolidays() {
        return includeHolidays;
    }

    public void setIncludeHolidays(boolean includeHolidays) {
        this.includeHolidays = includeHolidays;
    }
}
