package com.task.weekend.model.dto;

public class HolidayDto implements Comparable<HolidayDto> {

    private long id;

    private short day;

    private short month;

    private String description;

    public HolidayDto() {
    }

    public HolidayDto(Short day, Short month) {
        this.day = day;
        this.month = month;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public short getDay() {
        return day;
    }

    public void setDay(short day) {
        this.day = day;
    }

    public short getMonth() {
        return month;
    }

    public void setMonth(short month) {
        this.month = month;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(HolidayDto otherHolidayDto) {
        int cmp = (month - otherHolidayDto.month);
        if (cmp == 0) {
            cmp = (day - otherHolidayDto.day);
        }
        return cmp;
    }

    @Override
    public String toString() {
        return "HolidayDto{" +
                "id=" + id +
                ", day=" + day +
                ", month=" + month +
                ", description='" + description + '\'' +
                '}';
    }
}
