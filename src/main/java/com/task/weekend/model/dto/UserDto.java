package com.task.weekend.model.dto;

import org.hibernate.validator.constraints.Length;

import java.util.HashSet;
import java.util.Set;

public class UserDto {

    private long id;

    @Length(min = 6, max = 32, message = "{Length.userForm.login}")
    private String login;

    @Length(min = 8, message = "{Length.userForm.password}")
    private String password;

    private String confirmPassword;

    private Set<RoleDto> roleDtos = new HashSet<RoleDto>();

    public UserDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Set<RoleDto> getRoleDtos() {
        return roleDtos;
    }

    public void setRoleDtos(Set<RoleDto> roleDtos) {
        this.roleDtos = roleDtos;
    }
}
