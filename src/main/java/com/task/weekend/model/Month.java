package com.task.weekend.model;

import java.time.DateTimeException;

public enum Month {

    JANUARY("January"),

    FEBRUARY("February"),

    MARCH("March"),

    APRIL("April"),

    MAY("May"),

    JUNE("June"),

    JULY("July"),

    AUGUST("August"),

    SEPTEMBER("September"),

    OCTOBER("October"),

    NOVEMBER("November"),

    DECEMBER("December");

    private static final Month[] ENUMS = Month.values();

    private final String displayName;

    Month(String displayName) {
        this.displayName = displayName;
    }

    public static Month of(int month) {
        if (month < 1 || month > 12) {
            throw new DateTimeException("Invalid value for MonthOfYear: " + month);
        }
        return ENUMS[month - 1];
    }

    public int getValue() {
        return ordinal() + 1;
    }

    public String getDisplayName() {
        return displayName;
    }
}
