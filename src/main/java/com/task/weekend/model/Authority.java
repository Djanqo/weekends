package com.task.weekend.model;

public enum Authority {

    ROLE_USER,

    ROLE_ADMIN
}
