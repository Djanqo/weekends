package com.task.weekend.validator;

import com.task.weekend.model.dto.UserDto;
import com.task.weekend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return UserDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto userDto = (UserDto) o;

        UserDto userExists = userService.getByLogin(userDto.getLogin());

        if (userExists != null) {
            errors.rejectValue("login", "Duplicate.userForm.login");
        }

        if (!userDto.getConfirmPassword().equals(userDto.getPassword())) {
            errors.rejectValue("confirmPassword", "Different.userForm.passwordConfirm");
        }
    }
}