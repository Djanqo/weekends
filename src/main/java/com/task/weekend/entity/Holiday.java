package com.task.weekend.entity;

import javax.persistence.*;

@Entity
@Table(name = "holiday")
public class Holiday implements Comparable<Holiday>{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "day", nullable = false)
    private short day;

    @Column(name = "month", nullable = false)
    private short month;

    @Column(name = "description")
    private String description;

    public Holiday() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public short getDay() {
        return day;
    }

    public void setDay(short day) {
        this.day = day;
    }

    public short getMonth() {
        return month;
    }

    public void setMonth(short month) {
        this.month = month;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(Holiday otherHoliday) {
        int cmp = (month - otherHoliday.month);
        if (cmp == 0) {
            cmp = (day - otherHoliday.day);
        }
        return cmp;
    }
}
