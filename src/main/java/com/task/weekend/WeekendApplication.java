package com.task.weekend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.task.weekend")
@EnableAutoConfiguration
public class WeekendApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeekendApplication.class, args);
	}
}
