package com.task.weekend.converter;

import com.task.weekend.entity.Role;
import com.task.weekend.entity.User;
import com.task.weekend.model.dto.RoleDto;
import com.task.weekend.model.dto.UserDto;

import java.util.HashSet;
import java.util.Set;

import static com.task.weekend.converter.EntityDtoBasicConverter.convertBasicProperties;

public class UserDtoConverter {

    public static UserDto convertUserToUserDto(User user) {
        UserDto userDto = convertBasicProperties(user);

        Set<RoleDto> roleDtos = new HashSet<RoleDto>();
        for (Role role : user.getRoles()) {
            if (role != null) {
                roleDtos.add(convertBasicProperties(role));
            }
        }
        userDto.setRoleDtos(roleDtos);

        return userDto;
    }

    public static User convertUserDtoToUser(UserDto userDto) {
        User user = convertBasicProperties(userDto);

        Set<Role> roles = new HashSet<Role>();
        for (RoleDto roleDto : userDto.getRoleDtos()) {
            if (roleDto != null) {
                roles.add(convertBasicProperties(roleDto));
            }
        }
        user.setRoles(roles);

        return user;
    }
}
