package com.task.weekend.converter;

import com.task.weekend.entity.Holiday;
import com.task.weekend.model.dto.HolidayDto;

import static com.task.weekend.converter.EntityDtoBasicConverter.convertBasicProperties;

public class HolidayDtoConverter {

    public static HolidayDto convertHolidayToHolidayDto(Holiday holiday) {
        HolidayDto holidayDto = convertBasicProperties(holiday);

        return holidayDto;
    }

    public static Holiday convertHolidayDtoToHoliday(HolidayDto holidayDto) {
        Holiday holiday = convertBasicProperties(holidayDto);

        return holiday;
    }
}
