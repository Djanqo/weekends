package com.task.weekend.converter;

import com.task.weekend.entity.Role;
import com.task.weekend.entity.User;
import com.task.weekend.model.dto.RoleDto;
import com.task.weekend.model.dto.UserDto;

import java.util.HashSet;
import java.util.Set;

import static com.task.weekend.converter.EntityDtoBasicConverter.convertBasicProperties;

public class RoleDtoConverter {

    public static RoleDto convertRoleToRoleDto(Role role) {
        RoleDto roleDto = convertBasicProperties(role);

        return roleDto;
    }

    public static Role convertRoleDtoToRole(RoleDto roleDto) {
        Role role = convertBasicProperties(roleDto);

        return role;
    }
}
