package com.task.weekend.converter;

import com.task.weekend.entity.Holiday;
import com.task.weekend.entity.Role;
import com.task.weekend.entity.User;
import com.task.weekend.model.dto.HolidayDto;
import com.task.weekend.model.dto.RoleDto;
import com.task.weekend.model.dto.UserDto;

public class EntityDtoBasicConverter {
    public static User convertBasicProperties(UserDto userDto) {
        User user = new User();

        user.setId(userDto.getId());

        user.setLogin(userDto.getLogin());

        user.setPassword(userDto.getPassword());

        return user;
    }

    public static UserDto convertBasicProperties(User user) {
        UserDto userDto = new UserDto();

        userDto.setId(user.getId());

        userDto.setPassword(user.getPassword());

        userDto.setLogin(user.getLogin());

        return userDto;
    }

    public static Role convertBasicProperties(RoleDto roleDto) {
        Role role = new Role();

        role.setId(roleDto.getId());

        role.setName(roleDto.getName());

        return role;
    }

    public static RoleDto convertBasicProperties(Role role) {
        RoleDto roleDto = new RoleDto();

        roleDto.setId(role.getId());

        roleDto.setName(role.getName());

        return roleDto;
    }

    public static Holiday convertBasicProperties(HolidayDto holidayDto) {
        Holiday holiday = new Holiday();

        holiday.setId(holidayDto.getId());

        holiday.setDay(holidayDto.getDay());

        holiday.setMonth(holidayDto.getMonth());

        holiday.setDescription(holidayDto.getDescription());

        return holiday;
    }

    public static HolidayDto convertBasicProperties(Holiday holiday) {
        HolidayDto holidayDto = new HolidayDto();

        holidayDto.setId(holiday.getId());

        holidayDto.setDay(holiday.getDay());

        holidayDto.setMonth(holiday.getMonth());

        holidayDto.setDescription(holiday.getDescription());

        return holidayDto;
    }
}
