package com.task.weekend.service;

import com.task.weekend.model.dto.UserDto;

public interface UserService {

    UserDto getByLogin(String login);

    void createUser(UserDto userDto);


}
