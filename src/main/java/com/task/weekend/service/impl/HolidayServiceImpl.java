package com.task.weekend.service.impl;

import com.task.weekend.dao.HolidayRepository;
import com.task.weekend.model.Date;
import com.task.weekend.model.dto.HolidayDto;
import com.task.weekend.service.HolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.task.weekend.converter.HolidayDtoConverter.convertHolidayToHolidayDto;

@Service
public class HolidayServiceImpl implements HolidayService{

    @Autowired
    private HolidayRepository holidayRepository;

    @Override
    @Transactional(readOnly = true)
    public List<HolidayDto> getAll() {
        return holidayRepository.findAll().stream()
                .map((h) -> convertHolidayToHolidayDto(h)).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<HolidayDto> getHolidaysBetweenTwoDates(Date startDate, Date endDate) {
        short startDayOfMonth = (short) startDate.getDayOfMonth();
        short startMonth = (short) startDate.getMonthValue();
        short endDayOfMonth = (short) endDate.getDayOfMonth();
        short endMonth = (short) endDate.getMonthValue();
        if (!(startMonth == endMonth)) {
            return getAll().stream().filter((h) -> (h.getMonth() > startMonth && h.getMonth() < endMonth)
                    || (h.getMonth() == startMonth && h.getDay() >= startDayOfMonth)
                    || (h.getMonth() == endMonth && h.getDay() <= endDayOfMonth)).collect(Collectors.toList());
        }
        return getAll().stream().filter((h) -> h.getMonth() == startMonth)
                .filter((h) -> h.getDay() >= startDayOfMonth && h.getDay() <= endDayOfMonth)
                .collect(Collectors.toList());
    }
}
