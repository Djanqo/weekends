package com.task.weekend.service.impl;

import com.task.weekend.entity.Holiday;
import com.task.weekend.model.*;
import com.task.weekend.model.dto.HolidayDto;
import com.task.weekend.service.DateService;
import com.task.weekend.service.HolidayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DateServiceImpl implements DateService {

    @Autowired
    private HolidayService holidayService;

    @Override
    @Transactional(readOnly = true)
    public long getWeekendsBetweenTwoDates(CalculateWeekendsRequest calculateWeekendsRequest) {
        Date startDate = Date.of(calculateWeekendsRequest.getStartYear(),
                calculateWeekendsRequest.getStartMonth().getValue(),
                calculateWeekendsRequest.getStartDay());
        Date endDate = Date.of(calculateWeekendsRequest.getEndYear(),
                calculateWeekendsRequest.getEndMonth().getValue(),
                calculateWeekendsRequest.getEndDay());

        if (!endDate.isAfterOrEqual(startDate)) {
            throw new DateTimeException("The end date must go after the start date or be equal");
        }

        long totalWeekends = Date.weekendsBetweenTwoDates(startDate, endDate);
        if (!calculateWeekendsRequest.isIncludeHolidays()) {
            return totalWeekends;
        }

        int startYear = startDate.getYear();
        int endYear = endDate.getYear();

        if (startYear == endYear) {
            List<HolidayDto> holidayDtos = holidayService.getHolidaysBetweenTwoDates(startDate, endDate);
            totalWeekends += numberOfHolidaysOnWeekdays(startDate, endDate, holidayDtos);
        } else {
            Date lastDateOfStartYear = Date.of(startYear, Month.DECEMBER.getValue(), 31);
            List<HolidayDto> remainingHolidayDtos = holidayService
                    .getHolidaysBetweenTwoDates(startDate, lastDateOfStartYear);
            totalWeekends += numberOfHolidaysOnWeekdays(startDate, lastDateOfStartYear, remainingHolidayDtos);

            List<HolidayDto> allHolidaysDto = holidayService.getAll();
            int nextYear = startYear + 1;
            List<CashNumberOfHolidays> cashNumberOfHolidaysList = new ArrayList<CashNumberOfHolidays>();

            while (nextYear != endYear) {
                Date firstDateOfCurrentYear = Date.of(nextYear, Month.JANUARY.getValue(), 1);
                DayOfWeek dayOfWeekFirstDateOfCurrentYear = firstDateOfCurrentYear.getDayOfWeek();
                boolean currentYearIsLeap = Year.isLeap(nextYear);

                List<CashNumberOfHolidays> cashResultForCountYear = cashNumberOfHolidaysList.stream()
                        .filter((c) -> c.getJanuaryFirstDayOfWeek().equals(dayOfWeekFirstDateOfCurrentYear)
                                && !(c.isLeapYear() ^ currentYearIsLeap))
                        .collect(Collectors.toList());

                if (!cashResultForCountYear.isEmpty()) {
                    totalWeekends += cashResultForCountYear.stream().findFirst().get().getNumberOfHolidays();
                } else {
                    Date lastDateOfCurrentYear = Date.of(nextYear, Month.DECEMBER.getValue(), 31);
                    int numberOfHolidaysToAdd = numberOfHolidaysOnWeekdays(firstDateOfCurrentYear,
                            lastDateOfCurrentYear, allHolidaysDto);
                    cashNumberOfHolidaysList.add(new CashNumberOfHolidays(dayOfWeekFirstDateOfCurrentYear,
                            currentYearIsLeap, numberOfHolidaysToAdd));
                    totalWeekends += numberOfHolidaysToAdd;
                }
                nextYear++;
            }
            Date firstDateOfEndYear = Date.of(endYear, Month.JANUARY.getValue(), 1);
            List<HolidayDto> pastHolidayDtos = holidayService.getHolidaysBetweenTwoDates(firstDateOfEndYear, endDate);

            totalWeekends += numberOfHolidaysOnWeekdays(firstDateOfEndYear, endDate, pastHolidayDtos);
        }

        return totalWeekends;
    }

    @Override
    public int numberOfHolidaysOnWeekdays(Date startDate, Date endDate, List<HolidayDto> holidayDtos) {
        int result = 0;
        for (HolidayDto holidayDto : holidayDtos) {
            if (!((holidayDto.getDay() == 29 && holidayDto.getMonth() == 2) && !Year.isLeap(startDate.getYear()))) {
                DayOfWeek holidayDayOfWeek = Date.of(startDate.getYear(), holidayDto.getMonth(), holidayDto.getDay())
                        .getDayOfWeek();
                if (holidayDayOfWeek.getValue() < 6) {
                    result++;
                }
            }
        }
        return result;
    }


}
