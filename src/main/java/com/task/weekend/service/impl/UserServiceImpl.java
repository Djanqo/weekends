package com.task.weekend.service.impl;

import com.task.weekend.dao.RoleRepository;
import com.task.weekend.dao.UserRepository;
import com.task.weekend.entity.Role;
import com.task.weekend.entity.User;
import com.task.weekend.model.Authority;
import com.task.weekend.model.dto.UserDto;
import com.task.weekend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

import static com.task.weekend.converter.UserDtoConverter.convertUserDtoToUser;
import static com.task.weekend.converter.UserDtoConverter.convertUserToUserDto;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto getByLogin(String login) {
        User user = userRepository.findByLogin(login);
        return user == null ? null : convertUserToUserDto(user);
    }

    @Override
    @Transactional
    public void createUser(UserDto userDto) {
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByName(Authority.ROLE_USER.name()));
        User user = convertUserDtoToUser(userDto);
        user.setRoles(roles);
        userRepository.save(user);
    }
}
