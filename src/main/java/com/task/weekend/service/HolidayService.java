package com.task.weekend.service;

import com.task.weekend.model.Date;
import com.task.weekend.model.dto.HolidayDto;

import java.util.List;

public interface HolidayService {

    List<HolidayDto> getAll();

    List<HolidayDto> getHolidaysBetweenTwoDates(Date startDate, Date endDate);
}
