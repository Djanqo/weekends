package com.task.weekend.service;

import com.task.weekend.model.CalculateWeekendsRequest;
import com.task.weekend.model.Date;
import com.task.weekend.model.dto.HolidayDto;

import java.util.List;

public interface DateService {

    long getWeekendsBetweenTwoDates (CalculateWeekendsRequest calculateWeekendsRequest);

    int numberOfHolidaysOnWeekdays (Date startDate, Date endDate, List<HolidayDto> holidayDtos);
}
